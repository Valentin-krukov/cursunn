"use strict";

// 1 задача

// class Worker{
//     constructor(name,surname,rate,days){
//         this.name=name;
//         this.surname=surname;
//         this.rate=rate;
//         this.days=days;
//     }
//         getSalary(){
//             return (this.rate*this.days);
//         }
//         getFullName(){
//             return `${this.name},${this.surname}`;
//         }
// }
// const worker=new Worker('Иван', 'Иванов', 10, 31);

// console.log(worker.name); 
// console.log(worker.surname); 
// console.log(worker.getFullName()); 
// console.log(worker.rate); 
// console.log(worker.days); 
// console.log(worker.getSalary()); 

// // 2 задача

// class Boss extends Worker{
//     constructor(name,surname,rate,days,workers){
//         super(name,surname,rate,days);
//         this.workers=workers;
//     }
//     getSalary(){
//         return (this.rate*this.days*this.workers);
//     }
//     getFullName(){
//         return `${this.name},${this.surname}`;
//     }
// }

// const boss = new Boss('Иван', 'Иванов', 10, 31, 10);

// console.log(boss.name); 
// console.log(boss.surname);
// console.log(boss.getFullName());
// console.log(boss.rate); 
// console.log(boss.days); 
// console.log(boss.workers); 
// console.log(boss.getSalary()); 

// 3 задача

// class Worker {
//     #name;
//     #surname;
//     #rate;
//     #days;

//     constructor(name,surname,rate,days){
//         this.#name=name;
//         this.#surname=surname;
//         this.#rate=rate;
//         this.#days=days;
//     }
//     getName(){
//         return this.#name;
//      }
//       getSurname(){
//         return this.#surname;
//       }
//       getRate(){
//         return this.#rate;
//       }
//       getDays(){
//         return this.#days;
//       }
//       getSalary(){
//                 return (this.#rate*this.#days);
//             }

// }
// const worker=new Worker('Иван', 'Иванов', 10, 31);


// console.log(worker.getName()); 
// console.log(worker.getSurname()); 
// console.log(worker.getRate()); 
// console.log(worker.getDays()); 
// console.log(worker.getSalary()); 

// 4 задача
class Worker {
  constructor(name, surname, rate, days) {
      this.#rate=rate;
      this.#days = days;
      this.#name = name;
      this.#surname = surname;
  }
  #name;
  #surname;
  #rate;
  #days;
  get name() { return this.#name; }
  get surname() { return this.#surname; }
  get rate() { return this.#rate; }
  set rate(value) { this.#rate = value; }
  get days() { return this.#days; }
  set days(value) { this.#days = value; }
  getSalary() { return this.#rate * this.#days; }
}

let worker = new Worker('Иван', 'Иванов', 10, 31);
worker.rate = 100;
worker.days = 22;
console.log(worker.getSalary());
console.log(worker.name);
console.log(worker.surname);
console.log(worker.rate);
console.log(worker.days);

// 5 задача

class MyString{
  constructor(str){
    this.str=str;
  }

    reverse(){
      return this.str.split('').reverse().join('');    
    }

    ucFirst(){
      if (!this.str){
        return this.str;
      } else{
        return this.str[0].toUpperCase() + this.str.slice(1);
      }
    }
    ucWords(){
      let newStr = this.str.replace(/( |^)[а-яёa-z]/g, function(x){ return x.toUpperCase();});
      return newStr;
    
}
}
let str=new MyString('abcd');
let strNew=new MyString('abcd dgbd');
console.log(str.reverse());
console.log(str.ucFirst());
console.log(strNew.ucWords());

// 6 задача

class Validstor{
  constructor(eMail,domen,data,phone){
    this.eMail=eMail;
    this.domen=domen;
    this.data=data;
    this.phone=phone;
  }

  isEmail(){
    let reg=/^[a-z]+\@[a-z]+\.[a-z]+/gmi;
    if(reg.test(this.eMail)){
      return true;
    }else{
      return false;
    }
  }
  cheсkDomen(){
    let regPhone=/^[a0-z9]+\.[a-z]+/gmi;
    if(regPhone.test(this.domen)){
      return true;
    }else{
      return false;
    }
  }
  isDate(){
    let regData=/(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/gm;
    if(regData.test(this.data)){
      return true;
    }else{
      return false;
    }
  }
  isPhone(){
    let regPhone=/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/gm;
    if(regPhone.test(this.phone)){
      return true;
    }else{
      return false;
    }
  }
}
const user= new Validstor("abs@mail.ru","mail.ru","51.12.2021","+79997463275");
console.log(user.isEmail());
console.log(user.cheсkDomen());
console.log(user.isDate());
console.log(user.isPhone());

// 7 задача

class User{
  constructor(name,surname){
    this.name=name;
    this.surname=surname;
  }

  getFullName(){
    return `${this.name} ${this.surname}`;
  }
}

class Student extends User{
  constructor(name,surname,year){
    super(name,surname);
    this.year=year;
  }
  getCourse(){
    let todayYear=new Date().getFullYear();
    if(todayYear-this.year<=5){
      return `${todayYear-this.year} ${"курс"}`;
    }else{
      return "Учеба в университете закончилась";
    }
  }
}

let ivan=new Student("Ivan","Ivanov",2018);

console.log(ivan.getFullName());
console.log(ivan.getCourse());