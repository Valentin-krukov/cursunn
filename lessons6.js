"use strict";

// 1 задача

let vegetables = ['Капуста', 'Репа', 'Редиска', 'Морковка'];
let arrLength=vegetables.map(function (e) {
    return e.length;
});
console.log(arrLength);

// 2 задача

function arrSum(arr) {
    let result=[];  
    let newArr= arr.reduce(function(sum,current) {
        result.push(sum);
        return sum+current; 
    }); 
    result.push(newArr);
    return result;
}

console.log(arrSum([1,3,4,52,2,3]));

// 3 задача

function sumSeven(arr,n) {
   let sortArr=arr.sort(),
       newArr=[],
       oneEl=0,
       lastEl=arr.length-1;
            while(oneEl<lastEl){
                let sum=sortArr[oneEl]+sortArr[lastEl];
                    if(sum===n){
                        newArr.push(`${arr[oneEl]}:${arr[lastEl]}`);
                        oneEl+=1;
                        lastEl-=1;
                    }else if(sum<n){
                        lastEl+=1;
                    }else{
                        lastEl-=1;
                    }
            }
            return newArr;
   }
console.log(sumSeven([0, 1, 2, 3, 4, 5, 6, 7],7));

// 4 задача

function oneWords(str) {
    let array=str.split(" ").map(x=>x[0]).join("");
    return array;
}
console.log(oneWords("Каждый охотник желает знать, где сидит фазан"));

// 5 задача

function string(str) {
    let array=str.split(" "),
        newArr=[];
        for(let i=0;i<str.length;i++){
           let a= str.substring(-1+i,2+i);
            newArr.push(a); 
        }
           
    return newArr;
}
console.log(string('JavaScript'));

// 6 задача

function arrNumber(arr) {
     let arrSort=arr.sort();
     let newArr=[];
        for(let i=arr.length-1;i>=0;i-=1){
        newArr.push(arrSort[i]);
        }
        return newArr;   
}
console.log(arrNumber([5,7,2,9,3,1,8]));

// 7 задача

function threArr(arr1,arr2,arr3) {
  let sumArr= arr1.concat(arr2,arr3);
  let newArr = sumArr.sort().reverse().join(" ");
     return newArr;
}
console.log(threArr([1,2,3],[4,5,6],[7,8,9]));

// 8 задача

let arr = [[1, 2, 3], [4, 5], [6]];
console.log(arr.reduce((a, x) => a + x.reduce((aa, y) => aa + y, 0), 0));

// 9 задача

let arrThre = [[[1, 2],[3,4]],[[5,6],[7,8]]];
console.log(arrThre.reduce((a, x) => a + x.reduce((aa, y) => aa + y.reduce((aaa,z)=>aaa+z, 0), 0),0));

// 10 задача

function turnOverArr(arr) {
  let newArr=[],
      count=arr.length;
    for(let i=0;i<count;i++){
        newArr.push(arr.pop());
    }
    return newArr;
}
console.log(turnOverArr([1,2,3,4,5]));

// 11 задача

function sumArrNumb(arr) {
let sum = 0;
let count=0;
    for (let i = 0; i < arr.length; i++) {
        sum += arr[i];
        count=i;
    }
            if (sum > 10) {
            return((count - 1));
        }
}
console.log(sumArrNumb([1,5,3,5,23,4]));

// 12 задача

function arrayFill(el,n) {
    let arr=[];
        for(let i=0;i<n;i++){
            arr.push(el);
        }
        return arr;
}
console.log(arrayFill("x",5));
