// "use strict";

// // 1 задача

// function counter(a,b) {
//     console.log(a);
//     a--;
//     if(a>=b){
//         counter(a,b);
//     } else{
//         console.log("Время вышло");
//     }
// }
// counter(5,0);

// // 2 задача

// function f() {

//     alert( this ); // Выведет обьект null
//   }
//   f();
  
//   let user = {
  
//     g: f.bind(null)
  
//   };
  
//   user.g();

  // 3 задача

//   function f() {

//     alert(this.name);
//   }
//   f = f.bind( {name: "Вася"} ).bind( {name: "Петя" } );
//   f();//выведет Вася т.к. обьект при вызове функции запоминает контекст и аргументы только во время создания. 

//  // 4 задача

//  function sayHi() {
//     alert( this.name );
//   }
//   sayHi.test = 5;
//   let bound = sayHi.bind({
//     name: "Вася"
//   });
//   alert( bound.test ); // undefined т.к. у обьекта bound нет свойства тест.

//   5 задача
 
// function askPassword(ok, fail) {
//     let password = prompt("Password?", '');
//     if (password == "rockstar") ok();
//     else fail();
//   }
  
//   let user = {
//     name: 'Вася',
//     loginOk() {
//       alert(`${this.name} logged in`);
//     },
//     loginFail() {
//       alert(`${this.name} failed to log in`);
//     },
//   };
//   askPassword(user.loginOk.bind(user), user.loginFail.bind(user));

  //   6 задача

  // function askPassword(ok, fail) {

  //   let password = prompt("Password?", '');
  //   if (password == "rockstar") ok();
  //   else fail();
  // }
  // let user = {
  //   name: 'John',
  //   login(result) {
  //     alert( this.name + (result ? ' logged in' : ' failed to log in') );
  //   }
  // };
  // askPassword(user.login.bind(user.true),user.login.bind(user,false)); 

  //   8 задача

  var elem = {value: "Привет"};
function func(surname, name) {
    alert(this.value + ', ' + surname + ' ' + name);
}
//Тут напишите конструкцию с bind()
let nameOne =func.bind(elem,"Иванов",'Иван');
nameOne();
let nameTwo =func.bind(elem,"Петров",'Петр');
nameTwo();
// func('Иванов', 'Иван'); //тут должно вывести 'привет, Иванов Иван'
// func('Петров', 'Петр'); //тут должно вывести 'привет, Петров Петр'

//   9 задача

// let sum = (a, b, c) =>{
//   return b=>{
//     return c=>{
//      return a+b+c;
//     };
//   };
// };
// console.log(sum(1)(2)(3));

//   10 задача

// Напишите функцию которая будет складывать 2 числа. Используя bind вызовите ее  в контексте  
// другой функции, чтобы эта функция удваивала сумму 2-х элементов.

function sum(a,b){
  return this*(a+b);
}
const duble=sum.bind(2);
console.log(duble(3,4));