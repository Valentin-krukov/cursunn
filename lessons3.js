"use strict";
// 1 задача
/*let a=[1, 0, -3];
  for(let i=0; i<a.length; i+=1){
      if(a[i]==0){
          console.log("Верно");
      }else{
          console.log("Неверно");
      }
  }
  //2 задача
  let a=[1, 0, -3];
  for(let i=0; i<a.length; i+=1){
      if(a[i]>0){
          console.log("Верно");
      }else{
          console.log("Неверно");
      }
  }
//3 задача
let a=[1, 0, -3];
  for(let i=0; i<a.length; i+=1){
      if(a[i]<0){
          console.log("Верно");
      }else{
          console.log("Неверно");
      }
  }
//4 задача

let a=[1, 0, -3];
  for(let i=0; i<a.length; i+=1){
      if(a[i]>=0){
          console.log("Верно");
      }else{
          console.log("Неверно");
      }
  }
//5 задача
let a=[1, 0, -3];
  for(let i=0; i<a.length; i+=1){
      if(a[i]<=0){
          console.log("Верно");
      }else{
          console.log("Неверно");
      }
  }
//6 задача
let a=["test", "тест"];
  for(let i=0; i<a.length; i+=1){
      if(a[i]==="test"){
          console.log("Верно");
      }else{
          console.log("Неверно");
      }
  }*/
  //7 задача
  /*let a=["1", 1, 3];
  for(let i=0; i<a.length; i+=1){
      if(a[i]==="1"){
          console.log("Верно");
      }else{
          console.log("Неверно");
      }
  }*/
   //1 задача "Работа с логическими переменными"
  /* let test= true;
     if(test==true){
         console.log("Верно");
     }else {
        console.log("Не верно");
     }
     let test= false;
       test==true ? console.log("Верно") : console.log("Не верно");
       //2 задача
       let test= true; 
       if(test != true){
           console.log("Верно");
       }else {
          console.log("Не верно");
       }
      let test= false;
         test!=true ? console.log("Верно") : console.log("Не верно");*/
        
         //1 задача Работа с && (и) и || (или)

         /*let a=[5, 0, -3, 2];
         for(let i=0; i<a.length; i+=1){
             if(a[i]>0&&a[i]<5){
                 console.log(a[i]+" Верно");
             }else{
                 console.log(a[i] +" Неверно");
             }
         }
         //2 задача Работа с && (и) и || (или)

         let a=[5, 0, -3, 2];
         for(let i=0; i<a.length; i+=1){
             if(a[i]==0||a[i]==2){
                 a[i]=a[i]+7;
                 console.log(a[i]);
             }else{
                a[i]=a[i]/10;
                 console.log(a[i]);
             }
         }
         //3 задача Работа с && (и) и || (или)

         let a=[1, 0, 3],
             b=[3, 6, 5];
         for(let i=0; i<3; i+=1){
             if(a[i]<=1||b[i]>=3){
                 console.log(a[i]+b[i]);
             } else {
                console.log(a[i]-b[i]); 
             }
         }  

         //4 задача Работа с && (и) и || (или)

         let a=[1, 0, 3],
             b=[3, 6, 5];
         for(let i=0; i<3; i+=1){
             if(a[i]>2 && a[i]<11 || b[i]>=6 || b[i]<14){
                 console.log(a[i], b[i], "Верно");
             } else {
                console.log(a[i], b[i], "Не верно"); 
             }
         } 

         //1 задача На switch-case
         function randomInteger(min, max) {
            let num = min + Math.random() * (max+1- min);
            return Math.round(num);
         }
         let result;
         switch(randomInteger(1,4)){
             case 1:
             result="Зима";
             break;

             case 2:
             result="Весна";
             break;

             case 3:
             result="Лето";
             break;

             case 4:
             result="Лето";
             break;
         }
         console.log(result);

        //1 задача Общие задачи

 function randomInteger(min, max) {
     let a = min + Math.random() * (max+1- min);
     return Math.round(a);
 }
 let day=randomInteger(1,31);
 if(day>=1 && day<=10){
     console.log(day, " Первая декада");
 } else if(day>10 && day<=20){
    console.log(day, " Вторая декада");
 } else{
    console.log(day, " Третья декада");
 }
     //2 задача Общие задачи

     function randomInteger(min, max) {
        let a = min + Math.random() * (max+1- min);
        return Math.round(a);
    }
    let month=randomInteger(1,12);
    if(month==1 || month==2 || month==12){
        console.log(month, " Зима");
    } else if(month==3 || month==4 || month==5){
       console.log(month, " Весна");
    } else if(month==6 || month==7 || month==8){
        console.log(month, " Лето");
    }else if(month==9 || month==10 || month==11){
        console.log(month, " Осень");
    }

       //3 задача Общие задачи

let string="abcde";
  if(string.substring(0,1)=="a"){
console.log("Да");
  } else{
    console.log("Нет");
  }

  //4 задача Общие задачи

let str="12345",
    number= str.substring(0,1);
  if(number=="1" || number=="2" || number=="3"){
     console.log("Да");
  } else{
     console.log("Нет");
  }
    //5 задача Общие задачи

let strNumber="476",
    numberOne= +strNumber.substring(0,1),
    numberTwo= +strNumber.substring(1,2),
    numberThre= +strNumber.substring(2,3);
 console.log(numberOne+numberTwo+numberThre);

    //6 задача Общие задачи

    let strNumber="001001",
        numberOne= +strNumber.substring(0,1),
        numberTwo= +strNumber.substring(1,2),
        numberThre= +strNumber.substring(2,3),
        numberFor= +strNumber.substring(3,4),
        numberFive= +strNumber.substring(5,6),
        numberSix= +strNumber.substring(6,7);
    let summThriNumber=numberOne+numberTwo+numberThre;
    let summOtherNumber=numberFor+numberFive+numberSix;
        if(summThriNumber==summOtherNumber){
           console.log("Да");
        }else{
         console.log("Нет"); 
        }
 
         //1 задача Циклы while и for
let a=1;
  while(a<=100){
      console.log(a);
      a+=1;
  } 
for(let i=1; i<=100; i+=1){
    console.log(i);
}  
   //2 задача Циклы while и for

let b=11;
  while(b<=33){
    console.log(b);
    b+=1;
  } 

  for(let i=11; i<=33; i+=1){
    console.log(i);
} 

    //3 задача Циклы while и for

    let c=0;
    while(c<=100){
        if(c%2==0){
            console.log(c);  
        }
      c+=1;
    } 

    for(let i=0; i<=100; i+=1){
        if(i%2==0){
            console.log(i);  
        }
    } 
      //4 задача Циклы while и for

let d=0;
let result=0;
while(d<=100){
    result=result+d;
    d+=1;
}
console.log(result);

let resultFor=0;
for(let i=0; i<=100; i+=1){
      resultFor=resultFor+i;
    }
console.log(resultFor);

 //1 задача Работа с for для массивов

 let arr=[1, 2, 3, 4, 5];
    for(let i=0; i<arr.length; i+=1){
        console.log(arr[i]);
    }

//2 задача Работа с for для массивов

let arr=[1, 2, 3, 4, 5],
    result=0;
    for(let i=0; i<arr.length; i+=1){
       result=result+arr[i];
    }
    console.log(result);

    //1 задача Общие задачи

    let arr=[2, 5, 9, 15, 0, 4];
    for(let i=0; i<arr.length; i+=1){
        if(arr[i]>3 && arr[i]<10){
            console.log(arr[i]);
        }
     }

       //2 задача Общие задачи

       let arr=[2, -5, 9, -15, -9, -4];
       let result=0;
       for(let i=0; i<arr.length; i+=1){
           if(arr[i]>=0){
               result=result+arr[i];
           }
        }
        if(result>0){
            console.log(result);
        }else{
            console.log("Положительных чисел нет");
        }

        //3 задача Общие задачи

        let arr=[1, 2, 5, 9, 4, 13, 4, 10];
       for(let i=0; i<arr.length; i+=1){
           if(arr[i]==4){
               console.log("Есть!");
               break;
           }
        }

        //4 задача Общие задачи

        let arr=[10, 20, 30, 50, 235, 3000,];
        let b;
       for(let i=0; i<arr.length; i+=1){
           b=arr[i].toString();
           if(b[0]==1||b[0]==2||b[0]==5){
               console.log(arr[i]);
           }
       }

       //5 задача Общие задачи

       let arr=[1, 2, 3, 4, 5, 6, 7, 8, 9];
       let string=[];
         for(let i=0; i<arr.length; i+=1){
              string=string +"-"+arr[i];
         }
console.log(string+"-");

      //6 задача Общие задачи

      let arr=["Пн", "Вт", "Ср", "Чт", "Пт","Сб", "Вск"];
         for(let i=0; i<arr.length; i+=1){
            let html = arr[i];
            if (i > 4){
                 html = html.bold(); 
            }  
            const div = document.createElement('div');
            div.innerHTML = html;
            document.body.appendChild(div);
        }

        //7 задача Общие задачи

        let arr=["Вск", "Пн", "Вт", "Ср", "Чт", "Пт","Сб",];
        let date=new Date(),
            day=date.getDay();
         for(let i=0; i<arr.length; i+=1){
            let html = arr[i];
            if (i == day){
                 html = html.italics(); 
            }  
            const div = document.createElement('div');
            div.innerHTML = html;
            document.body.appendChild(div);
        }*/

        //8 задача Общие задачи

        let n=1000;
        let num=0;
        while(n>=50){
            n=n/2;
            num+=1;
        }
        console.log(n);
        console.log(num);