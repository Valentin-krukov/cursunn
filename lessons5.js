"use strict";

// 1 задача

function stringToarray(str) {
 let arr= str.split(" ");  
  return arr;
}
console.log(stringToarray("Каждый охотник желает знать"));

// 2 задача

function deleteCharacters(str, length) {
    if(length>str.length){
        return "В строке нет столько символов";
    }else{
        return str.substr(str,length);  
    }
}
console.log(deleteCharacters("Каждый охотник желает знать",5));

// 3 задача

function insertDash(str) {
  let newStr=str.replace(/ /g,"-");
   return newStr.toUpperCase();
}
console.log(insertDash("HTML JavaScript PHP"));

// 4 задача

function cursiveLetter(str){
    return str.charAt(0).toUpperCase() + str.slice(1);
}
console.log(cursiveLetter("string not starting with capital"));

// 5 задача

function capitalize(str) {
  return  str.replace(/(?:^|\s)\S/g, function(a) {
        return a.toUpperCase();
    });
}
console.log(capitalize("каждый охотник желает знать"));

// 6 задача

function changeRegister(str) {
    let newStr="";
    for(let i=0;i<str.length;i+=1){
        if(str[i]===str[i].toUpperCase()){
           newStr+=str[i].toLowerCase();
        }else{
             newStr+=str[i].toUpperCase();
        }
    }
    return newStr;
}

console.log(changeRegister("КаЖдЫй ОхОтНиК"));

// 7 задача

function removeChar(str) {
    str = str.replace(/[^a-zа-яё\s]/gi, '');
    return str;

} 
console.log(removeChar("every., -/ hunter #! wishes ;: {} to $ % ^ & * know"));

// 8 задача

function zeros (num, len, sign){
    let numStr=String(num);
    let zero=len-numStr.length;
    let numZeros="";
      for(zero;zero>0;zero--){
        numZeros+=0;
      }
      return(sign===undefined||sign==="")?numZeros+String(num):sign+numZeros+String(num);

}

console.log(zeros(20,4,"+"));

// 9 задача

function comparison(str1, str2){
    
    return str1.toUpperCase()===str2.toUpperCase();
    }
console.log(comparison("RkfbEk","fgjRJgd"));

// 10 задача

function insensitiveSearch(str1, str2) {
    let newStr1=str1.toUpperCase();
    let newStr2=str2.toUpperCase();
     return newStr1.includes(newStr2);
}
console.log(insensitiveSearch("МашинА едет По дОрогЕ","дороге"));

// 11 задача

function initCap(str){
    return str.toLowerCase().replace(/(?:^|\s)[a-z]/g, function(a) {
      return a.toUpperCase().replace(/\s+/g,"");    
    });
}

console.log(initCap("hEllow WorLd"));

// 12 задача

function initSnake(str){
    let newStr=str.replace(/[A-Z]/g, function(x) {
      return "_"+x.toLowerCase();  
    });
    return newStr.replace(/^_/,"");
}

console.log(initSnake("HelloWorld"));

// 13 задача

function repeatStr(str, n) {
    let neWstr="";
    for(let i=0;i<n;i+=1){
      neWstr+=" "+str;
    }
    return neWstr;
}
console.log(repeatStr("es",5));

// 14 задача

function path(pathname) {
    let namaP=pathname.match(/\w+\.\w+/g);
    return namaP;
}
console.log(path("/home/user/dir/file.txt"));

// 15 задача

let str = "Каждый охотник желает знать";
let str1 = "знать";
let str2 = "скрипт";
  console.log(str.endsWith(str1), str.endsWith(str2));

  // 16 задача

  function getSubstr(str, char, pos) {
     if(pos==="after"){
         return str.slice(str.indexOf(char)+char.length);
     } else if(pos==="before"){
        return str.slice(0,str.indexOf(char));
     }
  }
console.log(getSubstr("Сегодня идет дождь","идет","after"));

 // 17 задача

 function insert(str, substr, pos) {
    let arr=str.split(" ");
    arr.splice(pos,0,substr);
    return arr.join(" ");
}

console.log(insert("Ехал грека через реку","Волгу",4));

// 18 задача

function limitStr(str, n, symb) {
    let newStr="";
    if(str.length>n&&Boolean(symb)===true){
         newStr=`${str.substring(0,n)}${symb}`;
         return newStr;
    }else if(str.length>n&&Boolean(symb)===false){
        newStr=`${str.substring(0,n)}...`;
         return newStr;
    }else{
        return "Строка содержит маленькое колличество символов";
    }
}
console.log(limitStr("Гарри Поттер и филосовский камень",5,"!"));

// 19 задача

function cutString(str, n) {
  let arr=[];
  for(let i=0;i<str.length;i+=n){
      arr.push(str.substr(i,n));
  }
  return arr;
}
console.log(cutString("Выхухоль",2));

// 20 задача

function count(str, symb) {
    let re=new RegExp(symb,'ig');
    let arr=str.match(re);
return arr.length;
}
console.log(count("Машинаааа","а"));

// 21 задача

// function cutTegs(str) {
// return str.replace(/\>*\<*/g,"");
// }
// console.log(cutTegs('div class="informathion">Здесь важная информация</div><a href="#">0 тегах HTML.</a>'));

// 22 задача

function strip(str) {
     let arr=[];
    let newString=str.split(" ");
        for(let i=0;i<newString.length;i+=1){
            if(newString[i]!==""){
                 arr.push(newString[i]); 
            }
        }
    return arr.join(" ");
}
console.log(strip("  Ifmei   j  JFvFVN "));

// 23 задача

function cutString(str, n) {
    let arr=[],
        newStr=str.split(" ");
            newStr.splice(0,n);
         return newStr.join(" ");
}
console.log(cutString("Сила тяжести приложена к центру масс тела",2));

// 24 задача

function alphabetize(str) {
   let arr=str.split("");
    arr.sort();
    return arr.join("");
}
console.log(alphabetize("машина"));

// 25 задача

function findWord(word, str) {
    for(let i=0;i<str.split(" ").length;i+=1){
        if(str.split(" ")[i]===str){
            return true;
        }
    }
    return false;
}
console.log(findWord('abc def ghi jkl mno pqr stu','abc'));